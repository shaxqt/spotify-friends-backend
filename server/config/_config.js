// fill this file and change it's name to config.js
module.exports = {
  client_id: 'your id (from your spotify application)',
  client_secret: 'your secret (also from your spotify application)',
  redicret_uri_comment:
    'add the url to your application at developers.spotify.com',
  host: 'http://localhost:3333',
  mongoDB: 'mongodb://localhost:27017/spotifyFriends',
  tokenCookieName: 'spotify_friends_token',
  frontend: 'http://localhost:3000'
}
